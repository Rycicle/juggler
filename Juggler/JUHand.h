//
//  JUHand.h
//  Juggler
//
//  Created by Ryan Salton on 08/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef NS_ENUM(NSUInteger, HandType) {
    kHandLeft,
    kHandRight
};

@interface JUHand : SKSpriteNode

@property (nonatomic, assign) HandType handType;


+ (instancetype)spriteNodeWithImageNamed:(NSString *)name andType:(HandType)type;
- (void)activateHand;

@end
