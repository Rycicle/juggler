//
//  main.m
//  Juggler
//
//  Created by Ryan Salton on 08/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
