//
//  JUGameScene.m
//  Juggler
//
//  Created by Ryan Salton on 08/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "JUGameScene.h"
#import "JUHand.h"

#define HAND_POSITION_X 50
#define HAND_POSITION_Y 50

@interface JUGameScene ()

@property (nonatomic, strong) JUHand *leftHand;
@property (nonatomic, strong) JUHand *rightHand;

@property (nonatomic, assign) BOOL leftHandNext;

@end

@implementation JUGameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    self.backgroundColor = [UIColor orangeColor];
    
    self.leftHand = [JUHand spriteNodeWithImageNamed:@"hand" andType:kHandLeft];
    self.leftHand.position = CGPointMake(HAND_POSITION_X, HAND_POSITION_Y);
    [self addChild:self.leftHand];
    
    self.rightHand = [JUHand spriteNodeWithImageNamed:@"hand" andType:kHandRight];
    self.rightHand.position = CGPointMake(self.size.width - HAND_POSITION_X, HAND_POSITION_Y);
    [self addChild:self.rightHand];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if (self.leftHandNext)
        {
            [self.leftHand activateHand];
            self.leftHandNext = NO;
        }
        else
        {
            [self.rightHand activateHand];
            self.leftHandNext = YES;
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
