//
//  JUHand.m
//  Juggler
//
//  Created by Ryan Salton on 08/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "JUHand.h"

#define ANIM_DURATION 0.3

@interface JUHand ()

@property (nonatomic, strong) SKAction *moveHand;
@property (nonatomic, strong) SKAction *resetHand;

@end

@implementation JUHand

+ (instancetype)spriteNodeWithImageNamed:(NSString *)name andType:(HandType)type
{
    JUHand *hand = [super spriteNodeWithImageNamed:name];
    
    hand.texture = nil;
    hand.color = [UIColor greenColor];
    hand.size = CGSizeMake(50, 50);
    hand.handType = type;
    
    if (type == kHandLeft)
    {
        hand.moveHand = [SKAction moveByX:40 y:-15 duration:ANIM_DURATION];
    }
    else
    {
        hand.moveHand = [SKAction moveByX:-40 y:-15 duration:ANIM_DURATION];
    }
    
    return hand;
}

- (void)setPosition:(CGPoint)position
{
    [super setPosition:position];
    
    self.resetHand = [SKAction moveTo:CGPointMake(position.x, position.y) duration:0.1];
}

- (void)activateHand
{
    [self removeAllActions];
    
    [self runAction:self.resetHand completion:^{
        [self runAction:[SKAction sequence:@[self.moveHand, [self.moveHand reversedAction]]]];
    }];
}

@end
